//
//  ViewModelWithImageDelegate.swift
//  ProofOfConcept
//
//  Created by Rex Jason Alobba on 30/6/18.
//  Copyright © 2018 Rex Jason Alobba. All rights reserved.
//

import Foundation
import UIKit

protocol ViewModelWithImageDelegate {
    func setImage(image: UIImage)
}

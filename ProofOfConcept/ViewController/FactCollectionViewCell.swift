//
//  FactCollectionViewCell.swift
//  ProofOfConcept
//
//  Created by Rex Jason Alobba on 28/6/18.
//  Copyright © 2018 Rex Jason Alobba. All rights reserved.
//

import UIKit

class FactCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var title: UILabel!    
    
    @IBOutlet weak var imageView: UIImageView!        
    
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    
    var viewModel: FactCellViewModel? {
        didSet {
            title.text = viewModel?.titleText
            layoutImage()
            NotificationCenter.default.addObserver(self, selector: #selector(onImageLoaded), name: NSNotification.Name(rawValue: imageUpdatedNotification), object: nil)
        }
    }

    func layoutImage() {
        imageView.contentMode = .scaleAspectFit
        if(viewModel?.image != nil) {
            imageView.image = viewModel?.image
            imageHeight.constant = self.bounds.height * 0.75
        }
        else {
            imageHeight.constant = 0
        }
        self.layoutIfNeeded()
        self.setNeedsLayout()
    }
    
    @objc func onImageLoaded() {        
        layoutImage()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: imageUpdatedNotification), object: nil)
    }
}

//
//  ViewController.swift
//  ProofOfConcept
//
//  Created by Rex Jason Alobba on 28/6/18.
//  Copyright © 2018 Rex Jason Alobba. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        testRunData() // For debugging URL call
    }

    func testRunData() {
        let api = APIManager()
        api.loadData(CANADA_SOURCE, completion: didLoadData)
    }
    
    func didLoadData(_ heading: String, items: [Fact]) {
        for item in items {
            //For debugging only
//            print("title = \(item.title)")
//            print("description = \(item.description)")
//            print("imageHref = \(item.imageHref)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()        
    }


}


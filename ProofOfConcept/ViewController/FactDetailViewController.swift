//
//  FactDetailViewController.swift
//  ProofOfConcept
//
//  Created by Rex Jason Alobba on 2/7/18.
//  Copyright © 2018 Rex Jason Alobba. All rights reserved.
//

import UIKit

class FactDetailViewController: UIViewController {

    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var descLabel: UILabel!
            
    
    var viewModel: FactCellViewModel? {
        didSet {
            NotificationCenter.default.addObserver(self, selector: #selector(onImageLoaded), name: NSNotification.Name(rawValue: imageUpdatedNotification), object: nil)
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.all
    }

    @objc func onImageLoaded() {
        if let newImage = viewModel?.image {
            imageView.image = newImage
        }
        self.view.layoutIfNeeded()
    }
    
    
    func createOrientationConstraints() {
        //Setup constraints for different orientations
        let imageTopConstraintLandscape = NSLayoutConstraint(item: imageView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 74)
        let imageLeadingConstraintLandscape = NSLayoutConstraint(item: imageView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 10)
        
        let descLabelTopConstraintLandscape = NSLayoutConstraint(item: descLabel, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 74)
        let descLabelTrailingConstraintLandscape = NSLayoutConstraint(item: descLabel, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -10)
        let imageTrailingConstraintLandscape = NSLayoutConstraint(item: imageView, attribute: .trailing, relatedBy: .equal, toItem: descLabel, attribute: .leading, multiplier: 1, constant: -10)

        let imageLeadingConstraintPortrait = NSLayoutConstraint(item: imageView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 10)
        let imageTopConstraintPortrait = NSLayoutConstraint(item: imageView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 74)
        
        let descLabelTopConstraintPortrait = NSLayoutConstraint(item: descLabel, attribute: .top, relatedBy: .equal, toItem: imageView, attribute: .bottom, multiplier: 1, constant: 10)
        let descLabelLeadingConstraintPortrait = NSLayoutConstraint(item: descLabel, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 10)
        let descLabelTrailingConstraintPortrait = NSLayoutConstraint(item: descLabel, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -10)

        
        if let imageSize = viewModel?.imageSize {
            let ratio = imageSize.width / imageSize.height
            
            //Landscape constraints
            let imageWidthLandscape = UIScreen.main.bounds.width * 0.30
            let imageHeightLandscape = imageWidthLandscape / ratio
            
            let imageWidthConstraintLandscape = NSLayoutConstraint(item: imageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: imageWidthLandscape)
            let imageHeightConstraintLandscape = NSLayoutConstraint(item: imageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: imageHeightLandscape)
            
            
            //Portrait constraints
            let imageWidthPortrait = UIScreen.main.bounds.width * 0.90
            let imageHeightPortrait = imageWidthPortrait / ratio
            
            let imageWidthConstraintPortrait = NSLayoutConstraint(item: imageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: imageWidthPortrait)
            let imageHeightConstraintPortrait = NSLayoutConstraint(item: imageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: imageHeightPortrait)
            
            imageView.translatesAutoresizingMaskIntoConstraints = false
            descLabel.translatesAutoresizingMaskIntoConstraints = false
            
            
            if UIDevice.current.orientation.isLandscape {
                // Landscape mode
                imageWidthConstraintPortrait.isActive = false
                imageHeightConstraintPortrait.isActive = false
                imageTopConstraintPortrait.isActive = false
                imageLeadingConstraintPortrait.isActive = false
                descLabelTopConstraintPortrait.isActive = false
                descLabelLeadingConstraintPortrait.isActive = false
                descLabelTrailingConstraintPortrait.isActive = false
                
                imageWidthConstraintLandscape.isActive = true
                imageHeightConstraintLandscape.isActive = true
                imageTopConstraintLandscape.isActive = true
                imageLeadingConstraintLandscape.isActive = true
                descLabelTopConstraintLandscape.isActive = true
                descLabelTrailingConstraintLandscape.isActive = true
                imageTrailingConstraintLandscape.isActive = true
                
            } else {
                // Portrait mode
                imageWidthConstraintPortrait.isActive = true
                imageHeightConstraintPortrait.isActive = true
                imageTopConstraintPortrait.isActive = true
                imageLeadingConstraintPortrait.isActive = true
                descLabelTopConstraintPortrait.isActive = true
                descLabelLeadingConstraintPortrait.isActive = true
                descLabelTrailingConstraintPortrait.isActive = true
                
                imageWidthConstraintLandscape.isActive = false
                imageHeightConstraintLandscape.isActive = false
                imageTopConstraintLandscape.isActive = false
                imageLeadingConstraintLandscape.isActive = false
                descLabelTopConstraintLandscape.isActive = false
                descLabelTrailingConstraintLandscape.isActive = false
                imageTrailingConstraintLandscape.isActive = false
            }
        }
        else {
            imageView.frame = CGRect(x:0, y: 0, width: 0, height: 0)
            descLabel.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
    
    @objc func orientationChanged() {
        createOrientationConstraints()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(orientationChanged), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
        self.navigationItem.title = viewModel?.titleText
        
        imageView.contentMode = .scaleAspectFit
        imageView.image = viewModel?.image

        descLabel.text = viewModel?.descText
        
        createOrientationConstraints()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: imageUpdatedNotification), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
}

//
//  MainCollectionViewController.swift
//  ProofOfConcept
//
//  Created by Rex Jason Alobba on 28/6/18.
//  Copyright © 2018 Rex Jason Alobba. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class MainCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    let CELL_HEIGHT: CGFloat = 200

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    fileprivate struct storyboard {
        static let cellReuseIdentifier = "factsaboutcanada"
        static let segueIdentifier = "factdetail"
    }
    
    private let refreshControl = UIRefreshControl()
    
    private var viewModel: FactListViewModel = FactListViewModel(heading: "", items: []) {
        didSet {
            self.navigationItem.title = viewModel.title
            collectionView?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = false
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.color = UIColor.white
        self.activityIndicator.layer.zPosition = 100
        refreshControl.attributedTitle = NSAttributedString(string: "Pull down to refresh")
    
        
        //Receive image changes
        NotificationCenter.default.addObserver(self, selector: #selector(onImageLoaded), name: NSNotification.Name(rawValue: imageUpdatedNotification), object: nil)
        
        // Configure Refresh Control
        if #available(iOS 10.0, *) {
            collectionView?.refreshControl = refreshControl
        } else {
            collectionView?.addSubview(refreshControl)
        }        
        refreshControl.addTarget(self, action: #selector(refreshCollection(_:)), for: .valueChanged)
        runAPI()
    }
    
    @objc private func refreshCollection(_ sender: Any) {
        runAPI()
    }
    
    @objc private func onImageLoaded() {
        collectionView?.reloadData()
    }
    
    func runAPI() {
        self.activityIndicator.startAnimating()
        let api = APIManager()
        api.loadData(CANADA_SOURCE, completion: didLoadData)
    }

    func didLoadData(_ heading: String, items: [Fact]) {
        var filteredItems = [Fact]()
        for i in items {
            if i.title == nil && i.description == nil && i.imageHref == nil {
              continue
            }
            filteredItems.append(i)
        }
        viewModel = FactListViewModel(heading: heading, items: filteredItems)
        self.refreshControl.endRefreshing()
        self.activityIndicator.stopAnimating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: imageUpdatedNotification), object: nil)
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == storyboard.segueIdentifier {
            let cell = sender as! FactCollectionViewCell
            if let indexPath = self.collectionView!.indexPath(for: cell) {
                let factDetailViewController = segue.destination as! FactDetailViewController
                factDetailViewController.viewModel = viewModel.factList?[indexPath.row]
            }
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.factList!.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: storyboard.cellReuseIdentifier, for: indexPath) as! FactCollectionViewCell        
        cell.title.text = nil
        cell.imageView.image = nil
        cell.viewModel = viewModel.factList![indexPath.row] as FactCellViewModel
        return cell
    }

    // MARK: UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView,
                                 layout collectionViewLayout: UICollectionViewLayout,
                                 sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let factList = viewModel.factList {
            if factList.count > 0 {
                let cellViewModel = factList[indexPath.row]
                if let imageSize = cellViewModel.imageSize {
                    let ratio = imageSize.width / imageSize.height
                    let cellWidth = UIScreen.main.bounds.width * 0.95
                    let cellHeight = cellWidth / ratio
                    return CGSize(width: cellWidth + 20, height: cellHeight + 10)
                }
                else {
                    return CGSize(width: UIScreen.main.bounds.width * 0.95, height: UIScreen.main.bounds.width * 0.25)
                }
            }
        }
        return CGSize()
    }    
}

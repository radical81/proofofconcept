//
//  APIManager.swift
//  ProofOfConcept
//
//  Created by Rex Jason Alobba on 28/6/18.
//  Copyright © 2018 Rex Jason Alobba. All rights reserved.
//

import Foundation

class APIManager {
    func loadData(_ urlString: String, completion: @escaping (String, [Fact]) -> Void) {
        let config = URLSessionConfiguration.ephemeral
        let session = URLSession(configuration: config)
        guard let url = URL(string: urlString) else {
            print("Error: cannot create URL")
            return
        }
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) -> Void in
            
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            
            guard data != nil else {
                print("Error: No data in response")
                return
            }
            
            //Check for special characters like É
            guard let responseDataString = NSString(data: data!, encoding: String.Encoding.isoLatin1.rawValue) else {
                return
            }
            
            //Convert to UTF-8
            guard let responseData = responseDataString.data(using: String.Encoding.utf8.rawValue) else {
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let json = try decoder.decode(MyResponse.self, from: responseData)
                
                DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
                    DispatchQueue.main.async {
                        completion(json.title!, json.rows!)
                    }
                }
                
            } catch  let err {
                print("Err", err)
            }
        })
        task.resume()
    }
}

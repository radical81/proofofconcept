//
//  Fact.swift
//  ProofOfConcept
//
//  Created by Rex Jason Alobba on 28/6/18.
//  Copyright © 2018 Rex Jason Alobba. All rights reserved.
//

import Foundation

struct Fact: Codable {
    let title: String?
    let description: String?
    let imageHref: String?
    
    private enum CodingKeys: String, CodingKey {
        case title
        case description
        case imageHref
    }
}

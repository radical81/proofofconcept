//
//  MyResponse.swift
//  ProofOfConcept
//
//  Created by Rex Jason Alobba on 28/6/18.
//  Copyright © 2018 Rex Jason Alobba. All rights reserved.
//

import Foundation

struct MyResponse: Codable {
    let title: String?
    var rows: [Fact]?
    
    private enum CodingKeys: String, CodingKey {
        case title
        case rows
    }
}

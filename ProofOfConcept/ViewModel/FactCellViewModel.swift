//
//  FactCellViewModel.swift
//  ProofOfConcept
//
//  Created by Rex Jason Alobba on 30/6/18.
//  Copyright © 2018 Rex Jason Alobba. All rights reserved.
//

import Foundation
import UIKit

class FactCellViewModel: ViewModelWithImageDelegate {
    let titleText: String?
    let descText: String?
    let imageUrl: String?
    var image: UIImage?
    var imageSize: CGSize?

    init(fact:Fact) {
        self.titleText = fact.title
        self.descText = fact.description
        self.imageUrl = fact.imageHref
        if let imgHref = fact.imageHref {
            let imageHelper = FactImageHelper()
            imageHelper.imageReceiver = self
            imageHelper.getFactImage(imgUrl: imgHref)
        }
    }
    
    func setImage(image: UIImage) {        
        self.image = image
        self.imageSize = CGSize(width: image.size.width, height: image.size.height)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: imageUpdatedNotification), object: nil)
    }
}

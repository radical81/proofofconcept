//
//  FactListViewModel.swift
//  ProofOfConcept
//
//  Created by Rex Jason Alobba on 30/6/18.
//  Copyright © 2018 Rex Jason Alobba. All rights reserved.
//

import Foundation

class FactListViewModel {
    let title: String?
    let factList: [FactCellViewModel]?
    
    init(heading: String, items: [Fact]) {
        self.title = heading
        self.factList = [FactCellViewModel]()
        let rows = items as [Fact]
        for r in rows {
            self.factList?.append(FactCellViewModel(fact: r))
        }
    }
}

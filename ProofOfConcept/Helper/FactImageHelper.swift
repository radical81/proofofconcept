//
//  FactImageHelper.swift
//  ProofOfConcept
//
//  Created by Rex Jason Alobba on 30/6/18.
//  Copyright © 2018 Rex Jason Alobba. All rights reserved.
//

import Foundation
import UIKit

class FactImageHelper {
    var imageReceiver: ViewModelWithImageDelegate?
    
    func getFactImage(imgUrl: String) {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            var image: UIImage?
            let data = try? Data(contentsOf: URL(string: imgUrl)!)
            if data != nil {
                image = UIImage(data: data!)
            }
            //move back to Main Queue
            DispatchQueue.main.async {
                if image != nil {
                    self.imageReceiver?.setImage(image: image!)
                }
            }
        }
    }
}
